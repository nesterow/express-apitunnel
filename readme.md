ApiTunnel
-----------------
#### express-apitunnel
Expose local web services to remote express.js application


### About
**express-apitunnel** allows to expose a web service running on a local device to an express.js api.

##### Here are some wonderful things you can do with it:
- Control devices in your local network from anywhere
- Perform large tasks on a local machine (downloading files for example)
- Hook your IP camera feed to an express route
- You can hook more than one worker to one express route and the server will automatically balance load among them.
- You can turn on your teapot while you still at the office...
- ...


### Usage

```
Local Server -> Api Tunnel -> Public Server -> Browser/Clent
```

##### On a public server
```javascript
  const socketio = require('socket.io');
  const express = require('express');
  const app = express()
  const io = socketio(server)

  // Require server plugin
  const { apitunnel_server } = require('express-apitunnel');

  apitunnel_server(app, io, {

    endpoint: 'home-devices',// if not specified api route will be at '/unit/:unitname/*url'
                             // the api route will be at '/home-devices/:unitname/*url'
    units:{
      raspberry: 'qweqwe' // unit_name: access_token
    }
  })

  // your application logic ...
```

##### On a local device
```javascript

  const { apitunnel_client } = require('express-apitunnel');

  apitunnel_client({
    server: 'https://example.com', // your public server address
    unit: 'raspberry', // unit name
    token: 'very_secret', // access

    hostname: "torrent-downloader.local", // host to establish tunnel with
    pretend: "example.com", // assume a hostname in http header - Host: example.com
    port: 80,// port of the tunneled service
  })
  .then(data => {
      // connected
  })
  .catch(data => {
     // failed to connect
  })
```

##### On a client
```
  ~$ curl -i https://example.com/home-devices/raspberry/getTorrentList
  -# https://example.com/{endpoint}/{unit name}/{device url}

```

### Credits
  **express-apitunnel** is based on [Ericbarch's socket-tunnel](https://github.com/ericbarch/socket-tunnel)

### License
This project is licensed under the MIT License - see the LICENSE file for details
