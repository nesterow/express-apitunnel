const ss = require('socket.io-stream');
const uuid = require('uuid/v4');
const debug = require('debug')
const log = debug('tunnel')
const describe = debug('tunnel:describe')
debug.enable(`${debug.disable()},tunnel,tunnel:*`)

const BalancedSockets = {
  units: {},

  add(socket){
      describe('QUEUE: adding a new worker unit %o', socket.unit);
      let unit = socket.unit;
      this.units[unit] = this.units[unit] || [];
      let exists = this.units[unit].filter(s => s.uuid === socket.uuid)[0];
      if (exists) return describe('unit %o is aleready registered in the queue', socket.unit);
      this.units[unit].push(socket);
  },

  get(unit){

    if(!this.units[unit])
      return;

    setTimeout(()=>{
      let first = this.units[unit].shift();
      describe('QUEUE: putting worker %o:%o to the end of the que', unit, first.uuid);
      this.units[unit].push(first);
    })
    let first = this.units[unit][0];

    describe('QUEUE: unit %o:%o is going to take a job', unit, first.uuid);
    return first;
  },

  remove(socket){
    let unit = socket.unit;
    let id = socket.uuid;
    describe('QUEUE: removing %o:%o', unit, id);
    if (!this.units[unit])
      return describe('QUEUE: %o:%o does not exist and cannot be removed', unit, id);
    this.units[unit] = this.units[unit].filter(s => s.uuid !== id);
  }

}

module.exports = (app, io, config) => {

  log('initializing express-tunnel');
  app.all(`/${config.endpoint || 'unit'}/:unit/*`, (req, res)=>{
    get_tunnel(req,res).then((clientStream) => {
        let reqBody = [];

        req.on('data', (chunk) => {
          reqBody.push(chunk);
        });

        req.on('end', () => {
          let messageParts = get_http_header(req,res);
          if (reqBody.length > 0) {
            messageParts.push(Buffer.concat(reqBody).toString());
            messageParts.push('');
          }
          messageParts.push('');

          let message = messageParts.join('\r\n');
          clientStream.write(message);
        });
      }).catch((err) => {
        res.statusCode = 502;
        return res.end(err.message);
      });
  });


  function get_http_header (req) {
     let messageParts = [];
     let pretend = req.worker.pretend;
     messageParts.push(`${req.method} /${req.params[0]} HTTP/${req.httpVersion}`);
     for (let i = 0; i < (req.rawHeaders.length - 1); i += 2) {
       let value = req.rawHeaders[i + 1];
       let key = req.rawHeaders[i];
       if (key === 'Host')
          value = pretend || value;
       messageParts.push(key + ': ' + value);
     }
     messageParts.push('');
     return messageParts;
  }

  function get_tunnel (req,res) {
    return new Promise((resolve, reject) => {

      let unit = req.params.unit;
      if (!unit) return reject(new Error('Invalid unit name'));

      let socket = BalancedSockets.get(unit);
      if (!socket || !socket.authenticated) {
        return reject(new Error(`${unit} is currently unregistered or offline.`));
      }

      req.worker = socket;

      if (req.connection.clientStream !== undefined &&
          !req.connection.clientStream.destroyed) {
        return resolve(req.connection.clientStream);
      }

      let requestGUID = uuid();
      ss(socket).once(requestGUID, (clientStream) => {
        req.connection.clientStream = clientStream;
        clientStream.pipe(res.connection);
        resolve(clientStream);
        describe('APITUNNEL: procesing response %o of %o', '/'+req.params[0], req.params.unit);
      });
      describe('APITUNNEL: procesing request %o of %o','/'+req.params[0], req.params.unit);
      socket.emit('incoming-client', requestGUID);
    });
  }

  io.on('connection', (socket) => {

    socket.authenticated = false;
    setTimeout(()=> !socket.authenticated ? socket.disconnect('unathorized') : 0 ,1200)

    socket.on('create-tunnel', (opts, responseCb) => {
      if (socket.unit)
        return; // tunnel has already been created

      describe('APITUNNEL: a unit attemts to connect - %o',opts.unit);
      if(opts.token !== config.units[opts.unit]){
        describe('APITUNNEL: unit %o is unathorized - disconnecting',opts.unit);
        return socket.disconnect('unathorized');
      }
      describe('APITUNNEL: unit %o is accepted - proceeding',opts.unit);
      socket.authenticated = true;
      socket.unit = opts.unit;
      socket.uuid = uuid();
      socket.pretend = opts.pretend || opts.host;
      let name = socket.unit;

      BalancedSockets.add(socket);
      log('%o registered successfully', name)
      if (responseCb) {
        responseCb(null);
      }
    });

    // when a client disconnects, we need to remove their association
    socket.on('disconnect', () => {
      if (socket.unit) {
        BalancedSockets.remove(socket);
        log('%o:%o unregistered', socket.unit, socket.uuid)
      }
    });

  });


}
