const net = require('net');
const ioclient = require('socket.io-client');
const ss = require('socket.io-stream');

const debug = require('debug')
const log = debug('apitunnel')
const describe = debug('apitunnel:describe')
debug.enable(`${debug.disable()},apitunnel,apitunnel:*`)

const SOCKET_TIMEOUT = 1000 * 30;



module.exports = (options) => {
  return new Promise((resolve, reject) => {
    log('starting %o',options.unit);
    let socket = ioclient(options['server']);
    let stats = {
      name: options.unit,
      done: 0,
      fail: 0
    }
    process.on('exit', ()=>{
      log('STATS %o: %O', options.unit, stats);
    })
    socket.on('connect', () => {
      log('%o connected',options.unit);

      socket.emit('create-tunnel', {
        unit:options.unit,
        token:options.token,
        host: options.hostname,
        pretend: options.pretend
      }, (err) => {
        if (err) {
          log('%o [error] %o',options.unit, err);
          reject(err);
        } else {
          log('%o registered on motherhip',options.unit);

          let server = options['server'].toString();
          let url = `${server}/[unit-enpoint]/${options['unit']}`
          log('%o is now accesible at %o',options.unit, url);
          resolve(url);
        }
      });
    });

    socket.on('incoming-client', (client_id) => {
      let client = net.connect(options.port, options.hostname, () => {
        describe('%o is processing request [%o]',options.unit, client_id);
        let s = ss.createStream();
        s.pipe(client).pipe(s);

        s.on('end', () => {
          describe('%o finished request [%o]',options.unit, client_id);
          stats.done++;
          client.destroy();
        });

        ss(socket).emit(client_id, s);
      });

      client.setTimeout(SOCKET_TIMEOUT);
      client.on('timeout', () => {
        describe('%o - request [%o] is timed out',options.unit, client_id);
        client.end();
      });

      client.on('error', () => {
        // handle connection refusal (create a stream and immediately close it)
        let s = ss.createStream();
        log('%o error while processing request [%o]',options.unit, client_id);
        ss(socket).emit(client_id, s);
        stats.fail++;
        s.end();
      });
    });
  });
};
