/*
  Distribute load among unit with multiple workers
   ~$ node test/many_workers.js
*/

const socketio = require('socket.io');
const express = require('express');
const http = require('http');

const app = express();
const server = http.createServer(app);
const io = socketio(server);
const debug = require('debug');
const log = debug('test');
debug.enable(`${debug.disable()},test`);

//Remote Express Server
const { apitunnel_server } = require('..');
apitunnel_server(app, io, {
  units:{
    raspberry: 'qweqwe'
  }
})

// Emulate a heavy loaded resource
app.get('/data', (req,res)=>{
  setTimeout(()=>{
    res.end('OK');
  },1000)
})
server.listen(4646)


/*
  Local clients work in one unit
  Server will distribute requests among workers
   * currently server uses a cheap balancing model - it simply turns queue after each request
*/
const web = 'http://localhost:4646';
const host = 'localhost';
const { apitunnel_client } = require('..');
for(var i = 0; i < 12; i++){
  log('Creating worker %o', i + 1);
  apitunnel_client({
    server: web,
    unit: 'raspberry',
    token: 'qweqwe',
    port: 4646,
    hostname: host,
    pretend: host
  })
}

/*
  Making requests
*/

const request = require('request');
let req_status = {
  success: 0,
  failed: 0,
}
const req = ()=> new Promise((resolve, reject) => {
  log('REQUEST: Manking new request');
  request.get('http://localhost:4646/unit/raspberry/data/',function(err,res,body){
    if (err) {
      log('REQUEST failed with error {%o}', err);
      req_status.failed++;
      return reject(err)
    };
    if (res.statusCode !== 200) {
      log('REQUEST failed with status code {%o}', res.statusCode);
      req_status.failed++;
      return reject(res);
    }
    req_status.success++;
    resolve(res)
  });
});

const v8 = require('v8');
let iter = 0;
let int = setInterval(()=>{
  req()
  iter++
  if (iter > 2000){
    clearInterval(int);
    setTimeout(()=>{
      console.log('\n')
      console.log('\n')
      console.log('=========== STATUS ===========')
      console.log('------------------------------')
      log('REQUEST STATUS: %O', req_status)
      console.log('\n')
      console.log('\n')
      console.log('=========== MEMORY ===========')
      console.log('------------------------------')
      log('MEMORY USAGE: %O', v8.getHeapSpaceStatistics())
      console.log('\n')
      console.log('\n')
      console.log('=========== BALANCE ==========')
      console.log('------------------------------')
      process.exit(0)
    },3000);
  }
}, 10)
