/*
  1. ~$ node test/simple.js
  2. ~$ open http://localhost:4545/unit/raspberry/
*/

const FORWARD_HOST = 'ruby-hyperloop.org';

const debug = require('debug');

const socketio = require('socket.io');
const express = require('express');
const http = require('http');

const app = express()
const server = http.createServer(app)
const io = socketio(server)

//Remote Express Server
const { apitunnel_server } = require('..');
apitunnel_server(app, io, {
  units:{
    raspberry: 'qweqwe'
  }
})

server.listen(4545)

//Local client example
const { apitunnel_client } = require('..');
apitunnel_client({
  server: 'http://localhost:4545',
  unit: 'raspberry',
  token: 'qweqwe',
  port: 80,
  hostname: FORWARD_HOST,
  pretend: FORWARD_HOST
})
.then(data => {

})
.catch(data => {
  console.log('error', data)
})
